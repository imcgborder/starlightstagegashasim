﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace Carin.StarlightStageGashaSim.Console
{
    class Program
    {

        static void Main(string[] args)
        {
            var gasha = new Gasha();
            gasha.Execute().Wait();
        }
    }

    public class Gasha
    {
        private readonly List<string> ssrList = new List<string>()
        {
            "[ステージオブマジック]本田未央",
            "[ともだちたくさん]市原仁奈",
            "[自称・カンペキ]輿水幸子",
            "[キラデコ☆パレード]城ヶ崎莉嘉",
            "[ブライトメモリーズ]鷺沢文香",
            "[ノーブルヴィーナス]新田美波",
            "[ぐうたら王国]双葉杏",
            "[薔薇の闇姫]神崎蘭子",
            "[グレイトプレゼント]諸星きらり",
            "[ステージオブマジック]島村卯月",
            "[ステージオブマジック]渋谷凛",
            "[ステージオブマジック]速水奏",
            //限定(2倍にするためにまさかの2個投下といううんこソースの極み
            "[フルスイング☆エール]姫川友紀"
            "[フルスイング☆エール]姫川友紀"
        };

        private double ssrRate = 1.5;
        private int jewel = 0;
        private string requstSsr;

        const int GashaJewelPrice = 2500;

        public async Task Execute()
        {
            Console.WriteLine("★★★シンデレラガールズSSRかんたんシミュレーター★★★");

            while (true)
            {
                Console.WriteLine("SSRの確率を入れてね(%)：");
                if (double.TryParse(Console.ReadLine(), out ssrRate))
                {
                    break;
                }
            }

            while (true)
            {
                Console.WriteLine("溶かせるジュエルの数を入力してね：");
                if (int.TryParse(Console.ReadLine(), out jewel))
                {
                    break;
                }
            }

            while (true)
            {
                Console.WriteLine("ぜったいに欲しいSSRの名前を入力してね（部分入力可）：");
                requstSsr = Console.ReadLine();
                if (ssrList.Count(x => x.Contains(requstSsr)) > 0)
                {
                    break;
                }
            }

            var count = 1;
            while (true)
            {
                if (jewel < GashaJewelPrice)
                {
                    Console.WriteLine("おさいふが溶けました。");
                    break;
                }
                Console.Write($"{count}回目のガチャを回します：");
                jewel -= GashaJewelPrice;

                var findSsr = new ConcurrentBag<string>();
                var sequences = Enumerable.Range(0, 10);
                await Task.Delay(100);
                Parallel.ForEach(sequences, sequence =>
                {
                    var rand = new Random(Environment.TickCount * (sequence * 7)); // 7はてけとーsalt
                    if (rand.Next(0, 10000) < (ssrRate * 100))
                    {
                        findSsr.Add(ssrList[rand.Next(0, ssrList.Count())]);
                    }
                });

                if (findSsr.Count > 0)
                {
                    Console.WriteLine($"SSR: {string.Join(",", findSsr)} (残ジュエル: {jewel})");
                    if (findSsr.Count(x => x.Contains(requstSsr)) > 0)
                    {
                        Console.WriteLine($"★★{requstSsr}が引けました! おめでとうございます★★");
                        break;
                    }
                }
                else
                {
                    Console.WriteLine($"SSR: 0枚　 (残ジュエル: {jewel})");
                }
                count++;
            }

            Console.ReadLine();
        }
    }
}
